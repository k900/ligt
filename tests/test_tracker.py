import os
import unittest
import sh
from libligt.database import load_database
from libligt.tracker import Tracker


class StubObserver():
    got_started = False
    got_stopped = False

    def started(self, *_):
        self.got_started = True

    def stopped(self, *_):
        self.got_stopped = True


class BasicTests(unittest.TestCase):
    def test_start_stop(self):
        tracker = Tracker([])
        tracker.start()
        tracker.stop()
        self.assertFalse(Tracker.is_running)

    def test_database(self):
        data = load_database("tests/databases/sleep")
        self.assertEqual(data,
                         {
                             "sleep": [{
                                           "name": "Sleep",
                                           "description": "sleep - delay for a specified amount of time",

                                           "detection": [
                                               {
                                                   "platform": "linux",
                                                   "version": "native",
                                                   "executable": "sleep"
                                               }
                                           ]
                                       }]
                         })

    def test_observers(self):
        tracker = Tracker(load_database("tests/databases/sleep"))
        observer = StubObserver()
        tracker.observers.append(observer)
        tracker.start()
        sh.sleep('1')
        tracker.stop()
        self.assertTrue(observer.got_started and observer.got_stopped)

    def test_cmdline_direct(self):
        tracker = Tracker(load_database("tests/databases/bash"))
        observer = StubObserver()
        tracker.observers.append(observer)
        tracker.start()
        sh.bash(os.path.join(os.path.dirname(__file__), 'test-script.sh'))
        tracker.stop()
        self.assertTrue(observer.got_started and observer.got_stopped)


    def test_cmdline_indirect(self):
        tracker = Tracker(load_database("tests/databases/bash"))
        observer = StubObserver()
        tracker.observers.append(observer)
        tracker.start()
        sh.sh('-c', os.path.join(os.path.dirname(__file__), 'test-script.sh'))
        tracker.stop()
        self.assertTrue(observer.got_started and observer.got_stopped)

    def test_files(self):
        tracker = Tracker(load_database("tests/databases/files"))
        observer = StubObserver()
        tracker.observers.append(observer)
        tracker.start()
        cwd = os.path.join(os.path.dirname(__file__), 'testdir_files/')
        sh.bash(os.path.join(cwd, 'test-script.sh'), _cwd=cwd)
        tracker.stop()
        self.assertTrue(observer.got_started and observer.got_stopped)