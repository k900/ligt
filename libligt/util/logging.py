import inspect
import logging
import sys

logging.basicConfig(level=logging.DEBUG, format="<%(levelname)-8s> %(name)-20s: %(message)s")

#noinspection PyBroadException
def _logger():
    caller = inspect.stack()[2][0]
    tag = caller.f_globals.get("LOG_TAG", "unknown")
    tag = caller.f_locals.get("LOG_TAG", tag)

    if "self" in caller.f_locals:
        self = caller.f_locals["self"]
        if hasattr(self, "LOG_TAG"):
            tag = self.LOG_TAG
        else:
            tag = self.__class__.__name__

    return logging.getLogger(tag)


def error(message):
    _logger().exception(message)


def die(message):
    _logger().critical(message)
    sys.exit(1)


def info(message):
    _logger().info(message)


def warning(message):
    _logger().warning(message)


def debug(message):
    _logger().debug(message)