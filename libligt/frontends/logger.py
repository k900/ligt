from libligt.util import logging

__author__ = 'k900'


class Logger():
    def started(self, process, entry):
        logging.info(
            "Started {name} [exe={exe}, pid={pid}]: {desc}".format(name=entry["name"], exe=process.exe, pid=process.pid,
                                                                   desc=entry["description"]))

    def stopped(self, pid, entry, time):
        logging.info("Stopped {name} [pid={pid}]: {desc}, time: {time}".format(name=entry["name"], pid=pid,
                                                                               desc=entry["description"], time=time))