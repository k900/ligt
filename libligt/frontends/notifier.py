import notify2


class NotifierFrontend():
    def __init__(self):
        notify2.init("LiGT v0.1")

    def started(self, _, data):
        notify2.Notification("{game} started!".format(game=data["name"]), data["description"],
                             "notification-message-im").show()

    def stopped(self, _, data, time):
        notify2.Notification("{game} closed!".format(game=data["name"]), "You've played for {time}".format(time=time),
                             "notification-message-im").show()
