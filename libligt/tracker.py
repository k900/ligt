import datetime
import re
import os
import threading

import psutil

from libligt.util import logging


class Tracker():
    _timer = None

    observers = []

    _processes = None
    _tracked = {}

    _uid = os.getuid()

    is_running = False

    def list_processes(self):
        return {process for process in psutil.process_iter() if process.uids.real == self._uid}

    def check(self, process):
        try:
            exe = os.path.split(process.exe)[-1]
            if not exe in self._database:
                return

            for entry in self._database[exe]:
                for rule in entry["detection"]:
                    good = True
                    if "cmdline" in rule:
                        cmdline = " ".join(process.cmdline)
                        if not re.search(rule["cmdline"], cmdline):
                            good = False

                    for file in rule.get("files", ()):
                        path = file.format(cwd=process.getcwd(), exedir=os.path.dirname(process.exe))
                        if not os.path.exists(path):
                            good = False

                    if good:
                        logging.debug("Process {pid} matches data: {data}".format(pid=process.pid, data=rule))
                        return entry

            return None

        except (psutil._error.NoSuchProcess, psutil._error.AccessDenied):
            return None

    def __init__(self, database):
        logging.info("Initializing...")
        self._database = database
        self._processes = set(psutil.get_pid_list())
        self._timer = threading.Thread(target=self.check_processes)

    def start(self):
        logging.info("Starting...")
        self.is_running = True
        self._timer.start()

    def stop(self):
        logging.info("Stopping...")
        self.is_running = False
        self._timer.join()

    def check_processes(self):
        while self.is_running:
            new_processes = set(psutil.get_pid_list())

            started = set()
            for pid in new_processes - self._processes:
                try:
                    started.add(psutil.Process(pid))
                except psutil._error.NoSuchProcess:
                    pass

            stopped = self._processes - new_processes

            self._processes = new_processes

            for process in started:
                result = self.check(process)
                if result:
                    # Start tracking
                    self._tracked[process.pid] = {
                        "time": datetime.datetime.utcnow(),
                        "data": result
                    }

                    # Notify observers
                    for item in self.observers:
                        item.started(process, result)

                    break

            for pid in stopped:
                if pid in self._tracked:
                    # Get data
                    timedelta = datetime.datetime.utcnow() - self._tracked[pid]["time"]
                    data = self._tracked[pid]["data"]

                    # Notify observers
                    for item in self.observers:
                        item.stopped(pid, data, timedelta)

                    # Forget
                    del self._tracked[pid]
