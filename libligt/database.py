import json
import os
import sys
from libligt.util import logging

__author__ = 'k900'

LOG_TAG = "Database"


def load_database(path="ligtdb"):
    database = {}

    abspath = os.path.abspath(path)
    for file in os.listdir(abspath):
        if file.endswith(".json"):
            filename = os.path.join(abspath, file)
            if os.path.isfile(filename):
                entry = json.load(open(filename))
                for rule in entry["detection"]:
                    platform = "linux" if sys.platform == "linux2" else sys.platform
                    if rule.get("platform", platform) == platform:
                        if not rule["executable"] in database:
                            database[rule["executable"]] = []
                        database[rule["executable"]].append(entry)
                    else:
                        entry["detection"].remove(rule)

                logging.debug("Loaded game: {name}, {n} detection variants for platform.".format(name=entry["name"],
                                                                                                 n=len(entry[
                                                                                                     "detection"])))

    return database
