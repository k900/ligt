#!/usr/bin/env python

from libligt.database import load_database
from libligt.frontends.logger import Logger
from libligt.frontends.notifier import NotifierFrontend

from libligt.tracker import Tracker
from libligt.util import logging

LOG_TAG = "Main"

logging.info("Starting...")

tracker = Tracker(load_database())

tracker.observers.append(Logger())
tracker.observers.append(NotifierFrontend())

tracker.start()
try:
    while tracker.is_running:
        pass
except KeyboardInterrupt:
    tracker.stop()

logging.info("Bye!")